FROM nginx:alpine

WORKDIR /app

COPY CHECKS /app/CHECKS
COPY static /usr/share/nginx/html
